const data = require('../data.json');
const fs = require('fs');

module.exports = {
    index: (req, res) => {
        const products = data.products;
        
        return res.status(200).json({
            status: 'success',
            message: 'success get all data!',
            data: products
        });
    },
    getDetail: (req, res) => {
        const { productId } = req.params;

        let product1 = data.products;

        const product = product1.filter((el) => el.id == productId);

        if (product.length == 0) {
            return res.status(404).json({
                status: 'failed',
                message: 'not found',
                data: null
            });
        }

        return res.status(200).json({
            status: 'success',
            message: 'success get all data!',
            data: product[0]
        });
    },
    create: (req,res) => {
        const { name, description, price } = req.body;

        let product = data.products;

        const product1 = {
            id: product[product.length - 1].id + 1,
            name,
            description,
            price
        };
        product.push(product1);

        fs.writeFileSync('./data.json', JSON.stringify(data));

        return res.status(200).json({
            status: 'success',
            message: 'success get all data!',
            data: product
        });

    },
    update: (req, res) => {
        const { name, description, price } = req.body;
        const { productId } = req.params;

         let p = data.products;

        const foundIndex = p.findIndex((el) => el.id == productId);
        if (foundIndex < 0) {
            return res.status(404).json({
                status: 'failed',
                message: 'not found!',
                data: null
            });
        }

        if (name) {
            p[foundIndex].name = name;
        }

        if (description) {
            p[foundIndex].description = description;
        }

        if (price) {
            p[foundIndex].price = +price;
        }

        fs.writeFileSync('./data.json', JSON.stringify(data));

        return res.status(201).json({
            status: 'success',
            message: 'success create data!',
            data: p[foundIndex]
        });
    },
    delete: (req, res) => {
        const { productId } = req.params;

        let p = data.products;

        const foundIndex = p.findIndex((el) => el.id == productId);
        if (foundIndex < 0) {
            return res.status(404).json({
                status: 'failed',
                message: 'not found!',
                data: null
            });
        }

    p.splice(foundIndex, 1);

        fs.writeFileSync('./data.json', JSON.stringify(data));

        return res.status(200).json({
            status: 'success',
            message: 'success delete data!',
            data: p
        });

        

    }};
