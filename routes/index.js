const express = require('express');
const router = express.Router();
const user = require('./user.js');
const product = require('./product.js')

router.use('/users', user);
router.use('/products', product)

module.exports = router;